// import * as functions from 'firebase-functions';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as express from 'express';
import * as bodyParser from "body-parser";

const STRIPE_PUBLISHABLE_KEY='pk_live_51HSSCzCKgPdqvDgc3oUJLtmlpiBMWx7LiED7rGIKRCkFXAYylM3HgHk4LvQ1UcBHJToGZ0SFSok2GNEKhjVHDoTO00hFjRZeVP';
const STRIPE_SECRET_KEY='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';
const STRIPE_WEBHOOK_SECRET='whsec_1234';


admin.initializeApp(functions.config().firebase);
// const cors = ;
const app = express();
const main = express();

main.use('/', app);
main.use(bodyParser.json());

export const webApi = functions.https.onRequest(main);

app.get('/warmup', (request, response) => {

    response.send('Warming up friend.');

})


// const env = require("dotenv").config({ path: "./.env" });
const stripe = require("stripe")(STRIPE_SECRET_KEY);
// const express = require("express");
// const bodyParser = require("body-parser");
const webhookSecret = STRIPE_WEBHOOK_SECRET;
// const app = express();
// const { resolve } = require("path");


// Use JSON parser for all non-webhook routes
app.use((req, res, next) => {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  if (req.originalUrl === '/webhook') {
    next();
  } else {
    bodyParser.json()(req, res, next);
  }
});


app.get("/", (req, res) => {
  res.send("Hello from API");
});

app.get("/public-key", (req, res) => {
  res.send({ publicKey: STRIPE_PUBLISHABLE_KEY });
});

app.get("/product-details", (req, res) => {
  let data = getProductDetails();
  res.send(data);
});

app.post("/create-payment-intent", async (req, res) => {
  const body = req.body;
  const productDetails = getProductDetails();

  const options = {
    ...body,
    amount: productDetails[0].amount,
    currency: 'CAD'
  };

  try {
    const paymentIntent = await stripe.paymentIntents.create(options);
    res.json(paymentIntent);
  } catch (err) {
    res.json(err);
  }
});

const PRODUCTS = [
    {
        id: 1,
        amount: 25000,
        label: 'T-Shirt Natchav 1',
        photos: {},
        description: '',
        size: {},
        colors: {},
    },
    {
        id: 2,
        amount: 26000,
        label: 'T-Shirt Natchav 2',
        photos: {},
        description: '',
        size: {},
        colors: {},
    },
    {
        id: 3,
        amount: 26000,
        label: 'T-Shirt Natchav 3',
        photos: {},
        description: '',
        size: {},
        colors: {},
    },
  ];

let getProductDetails = () => {
  return PRODUCTS;
};

// Webhook handler for asynchronous events.
app.post('/webhook', bodyParser.raw({type: 'application/json'}), (req, res) => {
  let data;
  data = null;
  let eventType;
  // Check if webhook signing is configured.
  if (webhookSecret) {
    // Retrieve the event by verifying the signature using the raw body and secret.
    let event;
    const signature = req.headers["stripe-signature"];

    try {
      event = stripe.webhooks.constructEvent(
        req.body,
        signature,
        webhookSecret
      );
    } catch (err) {
      console.log(`⚠️ Webhook signature verification failed.`);
      res.sendStatus(400);
    }
    // Extract the object from the event.
    data = event.data;
    eventType = event.type;
  } else {
    // Webhook signing is recommended, but if the secret is not configured in `config.js`,
    // retrieve the event data directly from the request body.
    data = req.body.data;
    eventType = req.body.type;
    //return null;
  }

  if (eventType === "payment_intent.succeeded") {
    // Fulfill any orders, e-mail receipts, etc
    console.log("💰 Payment received!");
  }

  if (eventType === "payment_intent.payment_failed") {
    // Notify the customer that their order was not fulfilled
    console.log("❌ Payment failed.");
  }
  res.send(data);

  res.sendStatus(200);
});

// app.listen(4242, () => console.log(`Node server listening on port ${4242}!`));
