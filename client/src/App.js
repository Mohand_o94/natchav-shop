import React, {useReducer, useContext} from "react";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import DemoText from "./components/DemoText";
import CheckoutForm from "./components/CheckoutForm";
import Listing from "./components/Listing";

import tshirt from './assets/tshirt.png';
import tshirt2 from './assets/tshirt2.png';
import tshirt3 from './assets/tshirt3.png';
import logo from './assets/logo.png';

import api from "./api";

import "./App.css";

import Scroll from 'react-scroll';
import {reducer, initialState, Context} from './reducer';

var Link = Scroll.Link;
var DirectLink = Scroll.DirectLink;
var Element = Scroll.Element;
var Events = Scroll.Events;
var scroll = Scroll.animateScroll;
var scrollSpy = Scroll.scrollSpy;

const stripePromise = api.getPublicStripeKey().then(key => loadStripe(key));

export default function App() {

const [state, dispatch] = useReducer(reducer, initialState);
//const {state, dispatch} = useContext(Context);
console.log(state, 'ça passe');

  return (
    <div className="App ">
      {state.count}
      <header className="sr-header">
            <div className="sr-header__logo" />
          </header>
      <div className="sr-root">
      {/* <DemoText /> */}
      <div className="sr-content w3-panel w3-card">
        <Element name="test7" className="element" id="containerElement" style={{
            position: 'relative',
            height: '100%',
            overflow: 'scroll',
            margin: '20px',
          }}>
          <Listing state={state} dispatch={dispatch}/>

        </Element>
      </div>
        {/* <div className="sr-content w3-panel w3-card">
        Listing
          <div className="pasha-image-stack">
            <img
              alt=""
              src={tshirt}
              width="140"
              height="160"
            />
            <img
              alt=""
              src={tshirt2}
              width="140"
              height="160"
            />
            <img
              alt=""
              src={tshirt3}
              width="140"
              height="160"
            />
          </div>

          
        </div> */}
        <div className="checkout">
          <div className="sr-main w3-panel w3-card">
            <Elements stripe={stripePromise}>
              <CheckoutForm />
            </Elements>
          </div>
          <div className="sr-main">
          <h1> PANIER </h1>
          
          
            
            {state.panier.map((item) => {
              return (
                <div className=""> T-Shirt Natchav</div>
              )
            })}
          
          </div>
        </div>
      </div>
      {/* <DemoText /> */}
    </div>
  );
}
