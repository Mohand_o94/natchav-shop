import React from 'react';

const initialState = {
    count: 0,
    panier: [],

};

function reducer(state, action) {
  switch (action.type) {
    case 'increment':
      return {count: state.count + 1};
    case 'decrement':
      return {count: state.count - 1};
    case 'addPanier':
        return {panier: [action.value, ...state.panier]};
    default:
      throw new Error();
  }
}
  
const Context = React.createContext(initialState);

export {reducer, initialState, Context };