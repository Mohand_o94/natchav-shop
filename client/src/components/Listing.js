import React from 'react';
import './Listing.css';
import tshirt from '../assets/tshirt.png';
const Listing = ({state,dispatch}) => {
    const handleButton = () => {
        dispatch({type: 'addPanier', value: '12'});
    }
    return (
        <div className="listing"> 
            <div className="side-bar-product">
                <div className="side-item">
                    <img height="30px" src={tshirt}/>
                    <p>T-shirt Blanc</p>
                </div>
                <div className="side-item">
                    <img height="30px" src={tshirt}/>
                    <p>T-shirt Bleu</p>
                </div>
                <div className="side-item">
                    <img height="30px" src={tshirt} />
                    <p>T-shirt Noir</p>
                </div>
                <div className="side-item">
                    <img height="30px" src={tshirt} />
                    <p>T-shirt Gris</p>
                </div>
                <div className="side-item">
                    <img height="30px" src={tshirt} />
                    <p>T-shirt Vert</p>
                </div>
                <div className="side-item">
                    <img height="30px" src={tshirt} />
                    <p>T-shirt rouge</p>
                </div>
            </div>
            <div className="product">
                <div className="image-product">
                <img height="250px" src={tshirt} />
                    
                </div>
                <div className="desc-product">
                    <h1>T-shirt Natchav</h1>
                    <div className="action-product">
                        <span>25 $CAD</span>
                        <button onClick={handleButton}>Ajouter au panier</button>
                    </div>
                </div>
            </div>   

        </div>
    )
        
};

export default Listing;