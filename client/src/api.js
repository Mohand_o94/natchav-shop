import axios from 'axios';

const isLocal = true
const localPath = 'http://localhost:5001/payment-card-2l2w/us-central1/webApi';
const prodPath = 'https://us-central1-payment-card-2l2w.cloudfunctions.net/webApi';
const basePath = isLocal ? localPath : prodPath;

const createPaymentIntent = options => {
  return axios
    .post(`${basePath}/create-payment-intent`, {
      body: JSON.stringify(options)
    })
    .then(res => {
      if (res.status === 200) {
        return res.data;
      } else {
        return null;
      }
    })
    .then(data => {
      if (!data || data.error) {
        console.log("API error:", { data });
        throw new Error("PaymentIntent API Error");
      } else {
        return data.client_secret;
      }
    });
};

const getProductDetails = options => {
  return axios
    .get(`${basePath}/product-details`)
    .then(res => {
      if (res.status === 200) {
        return res.data;
      } else {
        return null;
      }
    })
    .then(data => {
      if (!data || data.error) {
        console.log("API error:", { data });
        throw Error("API Error");
      } else {
        return data;
      }
    });
};

const getPublicStripeKey = options => {
  return axios
    .get(`${basePath}/public-key`)
    .then(res => {
      if (res.status === 200) {
        return res.data;
      } else {
        return null;
      }
    })
    .then(data => {
      if (!data || data.error) {
        console.log("API error:", { data });
        throw Error("API Error");
      } else {
        return data.publicKey;
      }
    });
};

const api = {
  createPaymentIntent,
  getPublicStripeKey: getPublicStripeKey,
  getProductDetails: getProductDetails
};

export default api;
